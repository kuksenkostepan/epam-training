**histogram** - solution via "sed" script.

Run:

$ sed -nf histogram [file]

or

$ awk ' BEGIN { for (i=1;i<200;i++) print int (101*rand()) }' | sed -nf histogram
