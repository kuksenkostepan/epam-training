#!/bin/bash

#Variables

AllGroups=(Proj1_rw Proj1_r Proj2_rw Proj2_r Proj3_rw Proj3_r)

Projects=(Proj1 Proj2 Proj3)

AllUsers=(R1 R2 R3 R4 R5 I1 I2 I3 A1 A2 A3 A4)

Proj1_rw_Users=(R2 R3 R5 A1)
Proj1_r_Users=(A4)

Proj2_rw_Users=(R1 R5 A1)
Proj2_r_Users=(A2 A3)

Proj3_rw_Users=(R1 R2 R4 A2)
Proj3_r_Users=(A1 A4)


#Create groups

for item in ${AllGroups[@]}
do
	groupadd $item
done

#Create users

for item in ${AllUsers[@]}
do
	adduser -m $item
done

#Set groups for users


for item in ${Proj1_rw_Users[@]}
do 
	usermod -a -G Proj1_rw $item 
done

for item in ${Proj1_r_Users[@]} 
do
	usermod -a -G Proj1_r $item
done

for item in ${Proj2_rw_Users[@]}
do
	usermod -a -G Proj2_rw $item
done
 
for item in ${Proj2_r_Users[@]}
do
	usermod -a -G Proj2_r $item
done

for item in ${Proj3_rw_Users[@]}
do
	usermod -a -G Proj3_rw $item
done

for item in ${Proj3_r_Users[@]}
do
	usermod -a -G Proj3_r $item
done

#Create projects direcroties

for item in ${Projects[@]}
do
	mkdir $item
	chmod o-rx $item
done

#Set ACL

#--Proj1
setfacl -d -m g:Proj1_rw:rwx Proj1
setfacl -m g:Proj1_rw:rwx Proj1

setfacl -d -m g:Proj1_r:rx Proj1
setfacl -m g:Proj1_r:rx Proj1

#--Proj2
setfacl -d -m g:Proj2_rw:rwx Proj2
setfacl -m g:Proj2_rw:rwx Proj2

setfacl -d -m g:Proj2_r:rx Proj2
setfacl -m g:Proj2_r:rx Proj2

#--Proj3
setfacl -d -m g:Proj3_rw:rwx Proj3
setfacl -m g:Proj3_rw:rwx Proj3

setfacl -d -m g:Proj3_r:rx Proj3
setfacl -m g:Proj3_r:rx Proj3









