#!/bin/bash

#Create directories 
mkdir Proj1
mkdir Proj2
mkdir Proj3


#Create groups
groupadd managers

#Create users
adduser -m -G managers I1
adduser -m -G managers I2
adduser -m -G managers I3

#Set UAC permissions
chmod o-rx Proj1
chmod o-rx Proj2
chmod o-rx Proj3

chmod +t Proj1
chmod +t Proj2
chmod +t Proj3

#Set ACL
setfacl -d -m g:managers:rwx Proj1
setfacl -d -m g:managers:rwx Proj2
setfacl -d -m g:managers:rwx Proj3

setfacl -m g:managers:rwx Proj1
setfacl -m g:managers:rwx Proj2
setfacl -m g:managers:rwx Proj3

#Set sticky bit
chmod +t Proj1
chmod +t Proj2
chmod +t Proj3
