#!/bin/bash
#EPAM-Training 2019
#bash homework, task 1
#Stepan Kuksenko

function replace_func {
#!/bin/bash
#parse scripts options
template_file=""
result_file=""
while [[ -n "$1" ]]
    do
        case "$1" in
            -t|--template)
            #check for double option
            if [[ -z ${template_file} ]]; then
                #check file
                if [[ -e $2 ]]; then
                    #check file availability
                    if [[ -r $2 ]] ; then
                        template_file=$2
                    else
                       echo "File \"$2\" already exist but you don't have read permissions." >&2
                        exit 1
                    fi
                else
                    echo "Can't find template file \"$2\"." >&2
                    exit 1
                 fi
            else
                echo "Too much \"$1\" options, need only one." >&2
                exit 1
            fi
            shift ;;
            -r|--result)
            #check for double option
            if [[ -z ${result_file} ]]; then
                #check file
                if [[ -e $2 ]]; then
                    #is it file ?
                    if [[ -f $2 ]]; then
                        #check for write permissions
                        if [[ -w $2 ]]; then
                            result_file=$2
                        else
                            echo "File \"$2\" already exist but you don't have write permissions." >&2
                            exit 1
                        fi
                    else
                        echo "\"$2\" already exist but it isn't a file." >&2
                        exit 1
                    fi
                else
                    result_file=$2
                fi
            else
                echo "Too much \"$1\" options, need only one." >&2
                exit 1
            fi
            shift ;;
            *) echo "\"$1\" is no option" >&2 ;;
        esac
        shift
    done


#pattern replacement
#make clear result file
echo "" > ${result_file}
sed 1d ${result_file} > ${result_file}
#set IFS variable for work with lines in loop
IFS=$'\n'
#read templatefile by lines (with blank lines)
while read line
    do
        #calculate pattern numbers in line
        numbers_in_line=$(echo "$line" | grep -oE '{{{[^{}]*}}}' | wc -l)

        #loop for replace pattern to variable value for each entry
        for (( i=1; i <= $numbers_in_line; i++ ))
            do
                #separate the string with the first entry
                first_string=$(echo "$line" | sed -e 's/\(^[^{}]*{{{[^{}]*}}}\).*$/\1/')

                #remember the rest of the line
                second_string=$(echo "$line" | sed -e 's/^[^{}]*{{{[^{}]*}}}\(.*$\)/\1/')

               #extract the variable and change the pattern to its value
                first_string=$(echo "$first_string" | sed -e "s/{{{.*}}}/$(echo \
                    $(echo $(echo $(eval echo \$$(echo "$first_string" \
                    | sed -e 's/^.*{{{\(.*\)}}}.*$/\1/')) | sed -e 's!\\!\\\\!g' \
                    | sed -e 's!&!\\&!g')) | sed -e 's!/!\\/!g' )/")

                #return the old string with the replaced value of the first entry
                line=${first_string}${second_string}
            done
        #write to the result file
        echo ${line} >> ${result_file}
    done < ${template_file}

}

replace_func $@
