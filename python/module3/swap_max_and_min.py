def swap_max_and_min(list_x):

    #check intager
    for i in list_x:
        if not isinstance(i, int):
            raise TypeError

    #check uniquieness
    if not len(list_x) == len(set(list_x)):
        raise ValueError

    #swap elements
    min_value=min(list_x)
    max_value=max(list_x)
    list_x[list_x.index(max_value)]=min_value
    list_x[list_x.index(min_value)]=max_value

    return list_x
