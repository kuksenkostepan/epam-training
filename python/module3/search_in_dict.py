def search_in_dict(list_x,dict_x):
    #create empty list for results
    list_result=[]

    #find key and value in dictionary
    for key in dict_x:
        for i in list_x:
            #compare value
            if i == dict_x[key]:
                list_result.insert(len(list_result),dict_x[key])
            #compare key
            if i == key:
                list_result.insert(len(list_result),key)

    return set(list_result)

