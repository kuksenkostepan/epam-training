
def delay(func):
    import time
    from functools import wraps
    @wraps(func)
    def wait_please():
        time.sleep(3)
        func()
    return wait_please
