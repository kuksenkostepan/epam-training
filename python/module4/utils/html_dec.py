def italic(func):
    def wrapper():
        return f'<i>{func()}</i>'
    return wrapper


def bold(func):
    def wrapper():
        return f'<b>{func()}</b>'
    return wrapper


def underline(func):
    def wrapper():
        return f'<u>{func()}</u>'
    return wrapper
