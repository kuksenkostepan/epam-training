def raises(exception):
    def func_deco(func):
        def wrapper():
            try:
                func()
            except:
                raise exception
        return wrapper
    return func_deco

