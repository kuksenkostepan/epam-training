def multiple_in_range(first,last):
    return [item for item in range(first,last+1) if item % 7 == 0 and item % 5 != 0]
